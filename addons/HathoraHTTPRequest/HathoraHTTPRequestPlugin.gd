tool
extends EditorPlugin


func _enter_tree():
	add_custom_type("HathoraHTTPRequest", "HTTPRequest", preload("./HathoraHTTPRequest.gd"), preload("./HathoraPluginIcon.png"))


func _exit_tree():
	remove_custom_type("HathoraHTTPRequest")
