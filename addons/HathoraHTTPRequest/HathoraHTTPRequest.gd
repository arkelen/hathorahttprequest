extends HTTPRequest

class_name HathoraHttpRequest

###
# Signals to subscribe
###
signal hathora_request_error(error_type, error_data)
signal hathora_request_login_anonymous_result(login_anonymous_response_data)
signal hathora_request_login_nickname_result(login_nickname_response_data)
signal hathora_request_login_google_result(login_google_response_data)
signal hathora_request_get_ping_service_endpoints_result(get_ping_service_endpoints_response_data)
signal hathora_request_create_lobby_result(create_lobby_response_data)
signal hathora_request_list_active_public_lobbies_result(list_active_public_lobbies_response_data)
signal hathora_request_get_lobby_info_result(get_lobby_info_response_data)
signal hathora_request_get_running_processes_result(get_running_processes_response_data)
signal hathora_request_get_stopped_processes_result(get_stopped_processes_response_data)
signal hathora_request_get_process_info_result(get_process_info_response_data)
signal hathora_request_create_room_result(create_room_response_data)
signal hathora_request_get_room_info_result(get_room_info_response_data)
signal hathora_request_get_active_rooms_for_process_result(get_active_rooms_for_process_response_data)
signal hathora_request_get_inactive_rooms_for_process_result(get_inactive_rooms_for_process_response_data)
signal hathora_request_destroy_room_result()
signal hathora_request_suspend_room_result()
signal hathora_request_get_connection_info_result(get_connection_info_response_data)
signal hathora_request_update_room_config()


###
# Exported variables
###
# with_dev_endpoints : when activated the dev token is retrieved from operating system ENV var
export var with_dev_endpoints:bool = false setget set_with_dev_endpoints
# verbose : when activated any call to Hathora API endpoints will print request input and ouput data. Should be turned off as soon as coding and debug is finished
export var verbose:bool = false setget set_verbose
# headless_print : when activated any printed data will be routed to std err. Thus the linux server binary will display prints in Hathora Console.
export var headless_print:bool = false setget set_headless_print


###
# Variables
###
# authorization_dev_token : When with_dev_endpoints is activated, this variable contains the dev token requiered for each dev endpoints
var authorization_dev_token: String
# authorization_player_token : The player token sent back from a successful login. Used when creating lobbies.
var authorization_player_token: String = ""
# Container to store each request until it's processed
var request_buffer: Array = []

###
# Constants
###
# Hathora API base url
const api_url: String = "https://api.hathora.dev"
# Hathora API content type
const content_type_header: String = "Content-Type: application/json"
# Hathora API endpoints
const api_endpoint: Dictionary = {
	"LoginAnonymous": "/auth/v1/{appId}/login/anonymous",
	"LoginNickname": "/auth/v1/{appId}/login/nickname",
	"LoginGoogle": "/auth/v1/{appId}/login/google",
	"GetPingServiceEndpoints": "/discovery/v1/ping",
	"CreateLobby": "/lobby/v3/{appId}/create",
	"ListActivePublicLobbies": "/lobby/v3/{appId}/list/public",
	"GetLobbyInfoByRoomId": "/lobby/v3/{appId}/info/roomid/{roomId}",
	"GetLobbyInfoByShortCode": "/lobby/v3/{appId}/info/shortcode/{shortCode}",
	"GetRunningProcesses": "/processes/v1/{appId}/list/running",
	"GetStoppedProcesses": "/processes/v1/{appId}/list/stopped",
	"GetProcessInfo": "/processes/v1/{appId}/info/{processId}",
	"CreateRoom": "/rooms/v2/{appId}/create",
	"GetRoomInfo": "/rooms/v2/{appId}/info/{roomId}",
	"GetActiveRoomsForProcess": "/rooms/v2/{appId}/list/{processId}/active",
	"GetInactiveRoomsForProcess": "/rooms/v2/{appId}/list/{processId}/inactive",
	"DestroyRoom": "/rooms/v2/{appId}/destroy/{roomId}",
	"SuspendRoom": "/rooms/v2/{appId}/suspend/{roomId}",
	"GetConnectionInfo": "/rooms/v2/{appId}/connectioninfo/{roomId}",
	"UpdateRoomConfig": "/rooms/v2/{appId}/update/{roomId}",
}
# Response callbacks for each endpoints
var api_endpoint_response_callback: Dictionary = {
	"LoginAnonymous": funcref(self, "_login_anonymous_response"),
	"LoginNickname": funcref(self, "_login_nickname_response"),
	"LoginGoogle": funcref(self, "_login_google_response"),
	"GetPingServiceEndpoints": funcref(self, "_get_ping_service_endpoints_response"),
	"CreateLobby": funcref(self, "_create_lobby_response"),
	"ListActivePublicLobbies": funcref(self, "_list_active_public_lobbies_response"),
	"GetLobbyInfoByRoomId": funcref(self, "_get_lobby_info_response"),
	"GetLobbyInfoByShortCode": funcref(self, "_get_lobby_info_response"),
	"GetRunningProcesses": funcref(self, "_get_running_processes_response"),
	"GetStoppedProcesses": funcref(self, "_get_stopped_processes_response"),
	"GetProcessInfo": funcref(self, "_get_process_info_response"),
	"CreateRoom": funcref(self, "_create_room_response"),
	"GetRoomInfo": funcref(self, "_get_room_info_response"),
	"GetActiveRoomsForProcess": funcref(self, "_get_active_rooms_for_process_response"),
	"GetInactiveRoomsForProcess": funcref(self, "_get_inactive_rooms_for_process_response"),
	"DestroyRoom": funcref(self, "_destroy_room_response"),
	"SuspendRoom": funcref(self, "_suspend_room_response"),
	"GetConnectionInfo": funcref(self, "_get_connection_info_response"),
	"UpdateRoomConfig": funcref(self, "_update_room_config_response"),
}


###
# Enums
###
# Each type of errors HathoraHTTPRequest can raise
enum ERROR_TYPE {HATHORA_RESULT, JSON_PARSER, HTTP_REQUEST}
# Hathora room visibility enum
const VISIBILITY:Dictionary = {
	PRIVATE = "private",
	PUBLIC = "public",
	LOCAL = "local",
}
# Hathora region enum
const REGION:Dictionary = {
	SEATTLE = "Seattle",
	WASHINGTON_DC = "Washington_DC",
	CHICAGO = "Chicago",
	LONDON = "London",
	FRANKFURT = "Frankfurt",
	MUMBAI = "Mumbai",
	SINGAPORE = "Singapore",
	TOKYO = "Tokyo",
	SYDNEY = "Sydney",
	SAO_PAULO = "Sao_Paulo",
}
# Hathora room status enum
const STATUS:Dictionary = {
	SCHEDULING = "scheduling",
	ACTIVE = "active",
	SUSPENDED = "suspended",
	DESTROYED = "destroyed",
}
# Hathora server default ENV variables
const DEFAULT_VARIABLE:Dictionary = {
	HATHORA_APP_ID = "HATHORA_APP_ID",
	HATHORA_APP_SECRET = "HATHORA_APP_SECRET",
	HATHORA_PROCESS_ID = "HATHORA_PROCESS_ID",
	HATHORA_REGION = "HATHORA_REGION",
}



###
# HathoraHTTPRequest callback class
###

class Callback:
	var _object:Object
	var _function:String
	var _parameter
	func _init(object:Object, function:String, parameter = null) -> void:
		self._object = object
		self._function = function
		self._parameter = parameter


###
# Response data types
###

class LoginAnonymousResponseData:
	var token:String
	func _init(json_body:Dictionary) -> void:
		self.token = json_body["token"]
	func get_class() -> String:
		return "LoginAnonymousResponseData"


class LoginNicknameResponseData:
	var token:String
	func _init(json_body:Dictionary) -> void:
		self.token = json_body["token"]
	func get_class() -> String:
		return "LoginNicknameResponseData"


class LoginGoogleResponseData:
	var token:String
	func _init(json_body:Dictionary) -> void:
		self.token = json_body["token"]
	func get_class() -> String:
		return "LoginGoogleResponseData"


class LobbyData:
	var room_config
	var created_at:String
	var created_by:String
	var visibility:String
	var region:String
	var room_id:String
	var app_id:String
	func _init(json_body:Dictionary) -> void:
		self.room_config = json_body["roomConfig"]
		self.created_at = json_body["createdAt"]
		self.created_by = json_body["createdBy"]
		self.visibility = json_body["visibility"]
		self.region = json_body["region"]
		self.room_id = json_body["roomId"]
		self.app_id = json_body["appId"]
		self.short_code = json_body["shortCode"]
	func get_class() -> String:
		return "LobbyData"


class CreateLobbyResponseData extends LobbyData:
	func _init(json_body:Dictionary).(json_body) -> void:
		pass
	func get_class() -> String:
		return "CreateLobbyResponseData"


class GetLobbyInfoResponseData extends LobbyData:
	func _init(json_body:Dictionary).(json_body) -> void:
		pass
	func get_class() -> String:
		return "GetLobbyInfoResponseData"


class PingServiceEndpointData:
	var port:int
	var host:String
	var region:String
	func _init(json_body:Dictionary) -> void:
		self.port = json_body["port"]
		self.host = json_body["host"]
		self.region = json_body["region"]
	func get_class() -> String:
		return "PingServiceEndpointData"


class ExposedPortData:
	var host:String
	var name:String
	var port:int
	var transport_type:String
	func _init(json_body:Dictionary) -> void:
		self.host = json_body["host"]
		self.name = json_body["name"]
		self.port = json_body["port"]
		self.transport_type = json_body["transportType"]
	func get_class() -> String:
		return "ExposedPortData"


class GetConnectionInfoResponseData:
	var additional_exposed_ports:Array
	var exposed_port:ExposedPortData
	var status:String
	var room_id:String
	func _init(json_body:Dictionary) -> void:
		self.additional_exposed_ports = []
		for additional_exposed_port in json_body["additionalExposedPorts"]:
			self.additional_exposed_ports.push_back(ExposedPortData.new(additional_exposed_port))
		self.status = json_body["status"]
		if (self.status == STATUS.ACTIVE):
			self.exposed_port = ExposedPortData.new(json_body["exposedPort"])
		self.room_id = json_body["roomId"]
	func get_class() -> String:
		return "GetConnectionInfoResponseData"


class SetLobbyStateResponseData extends LobbyData:
	func _init(json_body:Dictionary).(json_body) -> void:
		pass
	func get_class() -> String:
		return "SetLobbyStateResponseData"


class ProcessInfoData:
	var egressed_bytes:int
	var active_connections:int
	var draining:bool
	var terminated_at:String
	var stopping_at:String
	var started_at:String
	var starting_at:String
	var rooms_per_process:int
	var additional_exposed_ports:Array
	var exposed_port:ExposedPortData
	var region:String
	var process_id:String
	var deployment_id:int
	var app_id:String
	func _init(json_body:Dictionary) -> void:
		self.egressed_bytes = json_body["egressedBytes"]
		self.active_connections = json_body["activeConnections"]
		self.draining = json_body["draining"]
		self.terminated_at = json_body["terminatedAt"] if not json_body["terminatedAt"] == null else ""
		self.stopping_at = json_body["stoppingAt"] if not json_body["stoppingAt"] == null else ""
		self.started_at = json_body["startedAt"] if not json_body["startedAt"] == null else ""
		self.starting_at = json_body["startingAt"]
		self.rooms_per_process = json_body["roomsPerProcess"]
		self.additional_exposed_ports = []
		for additional_exposed_port in json_body["additionalExposedPorts"]:
			self.additional_exposed_ports.push_back(ExposedPortData.new(additional_exposed_port))
		self.exposed_port = ExposedPortData.new(json_body["exposedPort"]) if not json_body["exposedPort"] == null else null
		self.region = json_body["region"]
		self.process_id = json_body["processId"]
		self.deployment_id = json_body["deploymentId"]
		self.app_id = json_body["appId"]
	func get_class() -> String:
		return "ProcessInfoData"


class AllocationData:
	var unscheduled_at:String
	var scheduled_at:String
	var process_id:String
	var room_allocation_id:String
	func _init(json_body:Dictionary) -> void:
		self.unscheduled_at = json_body["unscheduledAt"] if not json_body["unscheduledAt"] == null else ""
		self.scheduled_at = json_body["scheduledAt"]
		self.process_id = json_body["processId"]
		self.room_allocation_id = json_body["roomAllocationId"]
	func get_class() -> String:
		return "AllocationData"
		

class RoomData:
	var app_id:String
	var room_id:String
	var room_config
	var status:String
	var current_allocation
	func _init(json_body:Dictionary) -> void:
		self.app_id = json_body["appId"]
		self.room_id = json_body["roomId"]
		self.room_config = json_body["roomConfig"]
		self.status = json_body["status"]
		self.current_allocation = AllocationData.new(json_body["currentAllocation"]) if not json_body["currentAllocation"] == null else null
	func get_class() -> String:
		return "RoomData"


class RunningProcessData extends ProcessInfoData:
	var rooms:Array
	var total_rooms:int
	func _init(json_body:Dictionary).(json_body) -> void:
		self.rooms = []
		for room in json_body["rooms"]:
			self.rooms.push_back(RoomData.new(room))
		self.total_rooms = json_body["totalRooms"]
	func get_class() -> String:
		return "RunningProcessData"


class CreateRoomResponseData:
	var additional_exposed_ports:Array
	var exposed_port:ExposedPortData
	var status:String
	var room_id:String
	func _init(json_body:Dictionary) -> void:
		for additional_exposed_port in json_body["additionalExposedPorts"]:
			self.additional_exposed_ports.push_back(ExposedPortData.new(additional_exposed_port))
		self.exposed_port = ExposedPortData.new(json_body["exposedPort"]) if json_body.has("exposedPort") and not json_body["exposedPort"] == null else null
		self.status = json_body["status"]
		self.room_id = json_body["roomId"]
	func get_class() -> String:
		return "CreateRoomResponseData"


class GetRoomInfoResponseData extends RoomData:
	var allocations:Array
	func _init(json_body:Dictionary).(json_body):
		for allocation in json_body["allocations"]:
			self.allocations.push_back(AllocationData.new(allocation))
	func get_class() -> String:
		return "GetRoomInfoResponseData"


###
# HathoraHTTPRequest callback test
###

###
# HathoraHTTPRequest node functions
###

# Called when the node is created
func _init():
	connect("request_completed", self, "_on_request_completed")
	authorization_player_token = get_environment_variable("HATHORA_PLAYER_TOKEN")


# Called every frame
func _process(delta):
	if (not request_buffer.empty()):
		var current_request: Dictionary = request_buffer[0]
		if (not current_request.processing):
			_make_request(current_request.method, current_request.api_endpoint, current_request.query, current_request.headers, current_request.body)
			current_request.processing = true


# with_dev_endpoints variable setter
func set_with_dev_endpoints(value:bool) -> void:
	with_dev_endpoints = value
	if (with_dev_endpoints):
		authorization_dev_token = OS.get_environment("HATHORA_DEV_TOKEN")
		if (authorization_dev_token.empty()):
			printerr("HathoraHttpRequest: HATHORA_DEV_TOKEN env variable is not set")
	else:
		authorization_dev_token = ""


# verbose variable setter
func set_verbose(value:bool) -> void:
	verbose = value


# headless_print variable setter
func set_headless_print(value:bool) -> void:
	headless_print = value


# Store a request in a container to execute it as soon as the last request is done
func _register_request(callback, method, api_endpoint: String, query: Dictionary, headers: Array, body: Dictionary, signal_alt_callback:Callback) -> void:
	request_buffer.push_back({
		"processing": false,
		"method": method,
		"callback": callback,
		"api_endpoint": api_endpoint,
		"query": query,
		"headers": headers,
		"body": body,
		"signal_alt_callback": signal_alt_callback,
	})


# Prepare and execute a request
func _make_request(method, api_endpoint: String, query: Dictionary, headers: Array, body: Dictionary) -> void:
	# Prepare the query parameter string
	var query_string: String = ""
	if (not query.empty()):
		var http_client = HTTPClient.new()
		query_string = "?" + http_client.query_string_from_dict(query)
	# Prepare the data to send as request body
	var body_data = ""
	if (not body.empty()):
		# Add 'Content-Type' header
		headers.push_back(content_type_header)
		# Convert body func parameter data to json
		body_data = JSON.print(body)
	# Print request parameter if verbose mode is activated
	_print_request_data_info(api_url + api_endpoint + query_string, headers, method, body_data)
	# Execute the http+ssl request to Hathora endpoint with forged parameters
	var err = request(api_url + api_endpoint + query_string, headers, true, method, body_data)
	if (err != OK):
		var error_message:String
		match err:
			ERR_UNCONFIGURED:
				error_message = "ERR_UNCONFIGURED: not in the tree"
			ERR_BUSY:
				error_message = "ERR_BUSY: still processing previous request"
			ERR_INVALID_PARAMETER:
				error_message = "ERR_INVALID_PARAMETER: given string is not a valid URL format"
			ERR_CANT_CONNECT:
				error_message = "ERR_CANT_CONNECT: not using thread and the HTTPClient cannot connect to host"
			_:
				error_message = str(err) + ": request error"
		emit_signal("hathora_request_error", ERROR_TYPE.HTTP_REQUEST, error_message)
		request_buffer.remove(0)


# Called when a response is received from a request. It callbacks the correct response function to treat the returned data
func _on_request_completed(result, response_code, headers, body) -> void:
	if (result == RESULT_SUCCESS):
		if (response_code == 204):
			request_buffer[0].callback.call_func(response_code, headers, body, request_buffer[0].signal_alt_callback)
		else:
			var json = JSON.parse(body.get_string_from_utf8())
			if (json.error == OK):
				request_buffer[0].callback.call_func(response_code, headers, json.result, request_buffer[0].signal_alt_callback)
			else:
				emit_signal("hathora_request_error", ERROR_TYPE.JSON_PARSER, {"error_code": json.error, "error_string": json.error_string, "error_line": json.error_line})
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HTTP_REQUEST, result)
	request_buffer.remove(0)


###
# HathoraHTTPRequest functions corresponding to each API endpoints
###

func login_anonymous(app_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.LoginAnonymous.replace("{appId}", app_id)
	_register_request(api_endpoint_response_callback.LoginAnonymous, HTTPClient.METHOD_POST, endpoint, {}, [], {}, callback)


func login_nickname(app_id:String, nickname:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.LoginNickname.replace("{appId}", app_id)
	var body: Dictionary = {
		"nickname": nickname,
	}
	_register_request(api_endpoint_response_callback.LoginNickname, HTTPClient.METHOD_POST, endpoint, {}, [], body, callback)


func login_google(app_id:String, id_token:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.LoginGoogle.replace("{appId}", app_id)
	var body: Dictionary = {
		"idToken": id_token,
	}
	_register_request(api_endpoint_response_callback.LoginGoogle, HTTPClient.METHOD_POST, endpoint, {}, [], body, callback)


func get_ping_service_endpoints(callback:Callback = null) -> void:
	_register_request(api_endpoint_response_callback.GetPingServiceEndpoints, HTTPClient.METHOD_GET, api_endpoint.GetPingServiceEndpoints, {}, [], {}, callback)


func create_lobby(app_id:String, visibility:String, room_config, region:String, short_code:String = "", callback:Callback = null) -> void:
	var endpoint = api_endpoint.CreateLobby.replace("{appId}", app_id)
	var query: Dictionary = {}
	if (not short_code.empty()):
		query["shortCode"] = short_code
	var headers = ["Authorization: " + authorization_player_token]
	var body: Dictionary = {
		"visibility": visibility,
		"roomConfig": JSON.print(room_config),
		"region": region,
	}
	_register_request(api_endpoint_response_callback.CreateLobby, HTTPClient.METHOD_POST, endpoint, query, headers, body, callback)


func list_active_public_lobbies(app_id:String, region: String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.ListActivePublicLobbies.replace("{appId}", app_id)
	var query: Dictionary = {}
	if (not region.empty()):
		query["region"] = region
	_register_request(api_endpoint_response_callback.ListActivePublicLobbies, HTTPClient.METHOD_GET, endpoint, query, [], {}, callback)


func get_lobby_info_by_room_id(app_id:String, room_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetLobbyInfoByRoomId.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	_register_request(api_endpoint_response_callback.GetLobbyInfoByRoomId, HTTPClient.METHOD_GET, endpoint, {}, [], {}, callback)


func get_lobby_info_by_short_code(app_id:String, short_code:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetLobbyInfoByShortCode.replace("{appId}", app_id)
	endpoint = endpoint.replace("{shortCode}", short_code)
	_register_request(api_endpoint_response_callback.GetLobbyInfoByShortCode, HTTPClient.METHOD_GET, endpoint, {}, [], {}, callback)


func get_running_processes(app_id:String, region:String = "", callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetRunningProcesses.replace("{appId}", app_id)
	var query: Dictionary = {}
	if (not region.empty()):
		query["region"] = region
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetRunningProcesses, HTTPClient.METHOD_GET, endpoint, query, headers, {}, callback)


func get_stopped_processes(app_id:String, region:String = "", callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetStoppedProcesses.replace("{appId}", app_id)
	var query: Dictionary = {}
	if (not region.empty()):
		query["region"] = region
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetStoppedProcesses, HTTPClient.METHOD_GET, endpoint, query, headers, {}, callback)


func get_process_info(app_id:String, process_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetProcessInfo.replace("{appId}", app_id)
	endpoint = endpoint.replace("{processId}", process_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetProcessInfo, HTTPClient.METHOD_GET, endpoint, {}, headers, {}, callback)


func create_room(app_id:String, room_config, region: String, room_id:String = "", callback:Callback = null) -> void:
	var endpoint = api_endpoint.CreateRoom.replace("{appId}", app_id)
	var query: Dictionary = {}
	if (not room_id.empty()):
		query["roomId"] = room_id
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	var body: Dictionary = {
		"roomConfig": JSON.print(room_config),
		"region": region,
	}
	_register_request(api_endpoint_response_callback.CreateRoom, HTTPClient.METHOD_POST, endpoint, query, headers, body, callback)


func get_room_info(app_id:String, room_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetRoomInfo.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetRoomInfo, HTTPClient.METHOD_GET, endpoint, {}, headers, {}, callback)


func get_active_rooms_for_process(app_id:String, process_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetActiveRoomsForProcess.replace("{appId}", app_id)
	endpoint = endpoint.replace("{processId}", process_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetActiveRoomsForProcess, HTTPClient.METHOD_GET, endpoint, {}, headers, {}, callback)


func get_inactive_rooms_for_process(app_id:String, process_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetInactiveRoomsForProcess.replace("{appId}", app_id)
	endpoint = endpoint.replace("{processId}", process_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.GetInactiveRoomsForProcess, HTTPClient.METHOD_GET, endpoint, {}, headers, {}, callback)


func destroy_room(app_id:String, room_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.DestroyRoom.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.DestroyRoom, HTTPClient.METHOD_POST, endpoint, {}, headers, {}, callback)


func suspend_room(app_id:String, room_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.SuspendRoom.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	_register_request(api_endpoint_response_callback.SuspendRoom, HTTPClient.METHOD_POST, endpoint, {}, headers, {}, callback)


func get_connection_info(app_id:String, room_id:String, callback:Callback = null) -> void:
	var endpoint = api_endpoint.GetConnectionInfo.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	_register_request(api_endpoint_response_callback.GetConnectionInfo, HTTPClient.METHOD_GET, endpoint, {}, [], {}, callback)


func update_room_config(app_id:String, room_id:String, room_config, callback:Callback = null) -> void:
	var endpoint = api_endpoint.UpdateRoomConfig.replace("{appId}", app_id)
	endpoint = endpoint.replace("{roomId}", room_id)
	var headers = ["Authorization: " + "Bearer " + authorization_dev_token]
	var body: Dictionary = {
		"roomConfig": JSON.print(room_config),
	}
	_register_request(api_endpoint_response_callback.UpdateRoomConfig, HTTPClient.METHOD_POST, endpoint, {}, headers, body, callback)


###
# HathoraHTTPRequest callback functions corresponding to each API endpoints response
###

func _login_anonymous_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		authorization_player_token = json_body["token"]
		var login_anonymous_response_data:LoginAnonymousResponseData = LoginAnonymousResponseData.new(json_body)
		_print_response_data_info(login_anonymous_response_data, "login_anonymous_response_data")
		_emit_signal_or_call_defered("hathora_request_login_anonymous_result", login_anonymous_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _login_nickname_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		authorization_player_token = json_body["token"]
		var login_nickname_response_data:LoginNicknameResponseData = LoginNicknameResponseData.new(json_body)
		_print_response_data_info(login_nickname_response_data, "login_nickname_response_data")
		_emit_signal_or_call_defered("hathora_request_login_nickname_result", login_nickname_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _login_google_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		authorization_player_token = json_body["token"]
		var login_google_response_data:LoginGoogleResponseData = LoginGoogleResponseData.new(json_body)
		_print_response_data_info(login_google_response_data, "login_google_response_data")
		_emit_signal_or_call_defered("hathora_request_login_google_result", login_google_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_ping_service_endpoints_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_ping_service_endpoints_response_data:Array = []
		for ping_service_endpoint_data in json_body:
			get_ping_service_endpoints_response_data.push_back(PingServiceEndpointData.new(ping_service_endpoint_data))
		_print_response_data_info(get_ping_service_endpoints_response_data, "get_ping_service_endpoints_response_data")
		_emit_signal_or_call_defered("hathora_request_get_ping_service_endpoints_result", get_ping_service_endpoints_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _create_lobby_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 201):
		if (not _json_decode(json_body, "roomConfig")):
			return
		var create_lobby_response_data:CreateLobbyResponseData = CreateLobbyResponseData.new(json_body)
		_print_response_data_info(create_lobby_response_data, "create_lobby_response_data")
		_emit_signal_or_call_defered("hathora_request_create_lobby_result", create_lobby_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _list_active_public_lobbies_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var list_active_public_lobbies_response_data:Array = []
		for lobby_data in json_body:
			if (not _json_decode(lobby_data, "roomConfig")):
				return
			list_active_public_lobbies_response_data.push_back(LobbyData.new(lobby_data))
		_print_response_data_info(list_active_public_lobbies_response_data, "list_active_public_lobbies_response_data")
		_emit_signal_or_call_defered("hathora_request_list_active_public_lobbies_result", list_active_public_lobbies_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_lobby_info_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		if (not _json_decode(json_body, "roomConfig")):
			return
		var get_lobby_info_response_data:GetLobbyInfoResponseData = GetLobbyInfoResponseData.new(json_body)
		_print_response_data_info(get_lobby_info_response_data, "get_lobby_info_response_data")
		_emit_signal_or_call_defered("hathora_request_get_lobby_info_result", get_lobby_info_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_running_processes_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_running_processes_response_data:Array = []
		for running_process in json_body:
			for room in running_process["rooms"]:
				if (not _json_decode(room, "roomConfig")):
					return
			get_running_processes_response_data.push_back(RunningProcessData.new(running_process))
		_print_response_data_info(get_running_processes_response_data, "get_running_processes_response_data")
		_emit_signal_or_call_defered("hathora_request_get_running_processes_result", get_running_processes_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_stopped_processes_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_stopped_processes_response_data:Array = []
		for stopped_process in json_body:
			get_stopped_processes_response_data.push_back(ProcessInfoData.new(stopped_process))
		_print_response_data_info(get_stopped_processes_response_data, "get_stopped_processes_response_data")
		_emit_signal_or_call_defered("hathora_request_get_stopped_processes_result", get_stopped_processes_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_process_info_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_process_info_response_data:ProcessInfoData = ProcessInfoData.new(json_body)
		_print_response_data_info(get_process_info_response_data, "get_process_info_response_data")
		_emit_signal_or_call_defered("hathora_request_get_process_info_result", get_process_info_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _create_room_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 201):
		var create_room_response_data:CreateRoomResponseData = CreateRoomResponseData.new(json_body)
		_print_response_data_info(create_room_response_data, "create_room_response_data")
		_emit_signal_or_call_defered("hathora_request_create_room_result", create_room_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_room_info_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		if (not _json_decode(json_body, "roomConfig")):
			return
		var get_room_info_response_data:GetRoomInfoResponseData = GetRoomInfoResponseData.new(json_body)
		_print_response_data_info(get_room_info_response_data, "get_room_info_response_data")
		_emit_signal_or_call_defered("hathora_request_get_room_info_result", get_room_info_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_active_rooms_for_process_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_active_rooms_for_process_response_data:Array = []
		for active_room in json_body:
			if (not _json_decode(active_room, "roomConfig")):
				return
			get_active_rooms_for_process_response_data.push_back(RoomData.new(active_room))
		_print_response_data_info(get_active_rooms_for_process_response_data, "get_active_rooms_for_process_response_data")
		_emit_signal_or_call_defered("hathora_request_get_active_rooms_for_process_result", get_active_rooms_for_process_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_inactive_rooms_for_process_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_inactive_rooms_for_process_response_data:Array = []
		for inactive_room in json_body:
			if (not _json_decode(inactive_room, "roomConfig")):
				return
			get_inactive_rooms_for_process_response_data.push_back(RoomData.new(inactive_room))
		_print_response_data_info(get_inactive_rooms_for_process_response_data, "get_inactive_rooms_for_process_response_data")
		_emit_signal_or_call_defered("hathora_request_get_inactive_rooms_for_process_result", get_inactive_rooms_for_process_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _destroy_room_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 204):
		_emit_signal_or_call_defered("hathora_request_destroy_room_result", null, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _suspend_room_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 204):
		_emit_signal_or_call_defered("hathora_request_suspend_room_result", null, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _get_connection_info_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 200):
		var get_connection_info_response_data:GetConnectionInfoResponseData = GetConnectionInfoResponseData.new(json_body)
		_print_response_data_info(get_connection_info_response_data, "get_connection_info_response_data")
		_emit_signal_or_call_defered("hathora_request_get_connection_info_result", get_connection_info_response_data, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


func _update_room_config_response(response_code:int, headers:Array, json_body, signal_alt_callback:Callback) -> void:
	if (response_code == 204):
		_emit_signal_or_call_defered("hathora_request_update_room_config", null, signal_alt_callback)
	else:
		emit_signal("hathora_request_error", ERROR_TYPE.HATHORA_RESULT, json_body)


###
# HathoraHTTPRequest tool functions
###

func get_environment_variable(env_var_name:String) -> String:
	return OS.get_environment(env_var_name)


func _emit_signal_or_call_defered(signal_name:String, response_data, callback:Callback) -> void:
	if (callback and is_instance_valid(callback._object) and callback._object.has_method(callback._function)):
		if (callback._parameter):
			if (response_data):
				callback._object.call_deferred(callback._function, response_data, callback._parameter)
			else:
				callback._object.call_deferred(callback._function, callback._parameter)
		elif (response_data):
			callback._object.call_deferred(callback._function, response_data)
		else:
			callback._object.call_deferred(callback._function)
	elif (response_data):
		emit_signal(signal_name, response_data)
	else:
		emit_signal(signal_name)


func _json_decode(json_data, key:String) -> bool:
	if (json_data[key] == null):
		return true
	var json:JSONParseResult = JSON.parse(json_data[key])
	if (json.error == OK):
		json_data[key] = json.result
		return true
	emit_signal("hathora_request_error", ERROR_TYPE.JSON_PARSER, {"error_code": json.error, "error_string": json.error_string, "error_line": json.error_line})
	return false


func _print_if_verbose(arg0 = null, arg1 = null, arg2 = null, arg3 = null, arg4 = null, arg5 = null, arg6 = null, arg7 = null, arg8 = null, arg9 = null):
	if (verbose):
		arg0 = arg0 if arg0 != null else ""
		arg1 = arg1 if arg1 != null else ""
		arg2 = arg2 if arg2 != null else ""
		arg3 = arg3 if arg3 != null else ""
		arg4 = arg4 if arg4 != null else ""
		arg5 = arg5 if arg5 != null else ""
		arg6 = arg6 if arg6 != null else ""
		arg7 = arg7 if arg7 != null else ""
		arg8 = arg8 if arg8 != null else ""
		arg9 = arg9 if arg9 != null else ""
		if (headless_print):
			printerr (arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)
		else:
			print (arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)


func _print_request_data_info(url:String, headers:Array, method:int, data:String) -> void:
	_print_if_verbose("-- Request data --")
	_print_if_verbose("url = ", url)
	_print_if_verbose("headers = ", headers)
	match method:
		HTTPClient.METHOD_GET:
			_print_if_verbose("method = GET")
		HTTPClient.METHOD_POST:
			_print_if_verbose("method = POST")
		_:
			_print_if_verbose("method = unsupprted")
	_print_if_verbose("data = ", data)
	_print_if_verbose("")


func _print_response_data_info(data, data_name, tabulation_level = "\t") -> void:
	if (tabulation_level == "\t"):
		_print_if_verbose("-- Response data schema and content --")
	var property_data_typeof:int = typeof(data)
	if (property_data_typeof == TYPE_ARRAY):
		_print_if_verbose(data_name, ":Array, size is ", data.size())
		for i in range(data.size()):
			if (typeof(data[i]) == TYPE_OBJECT):
				_print_data_class_info(data[i], tabulation_level + "[" + str(i) + "]", tabulation_level + "\t")
			else:
				_print_if_verbose(tabulation_level + "\t", "[", i, "] : ", data[i])
	else:
		_print_data_class_info(data, data_name, tabulation_level)
	if (tabulation_level == "\t"):
		_print_if_verbose("")


func _print_data_class_info(data, data_name, tabulation_level = "\t") -> void:
	var properties = data.get_property_list()
	_print_if_verbose(data_name, ":", data.get_class())
	for property in properties:
		if ((property.type == TYPE_NIL and typeof(data.get(property.name)) == TYPE_NIL) or (property.type == TYPE_OBJECT and property.name == "script")):
			continue
		var property_data_str:String = str(data.get(property.name))
		var property_data_typeof:int = typeof(data.get(property.name))
		var property_data_typeof_str:String
		match property_data_typeof:
			TYPE_BOOL:
				property_data_typeof_str = "bool"
			TYPE_INT:
				property_data_typeof_str = "int"
			TYPE_REAL:
				property_data_typeof_str = "float"
			TYPE_STRING:
				property_data_typeof_str = "String"
			TYPE_VECTOR2:
				property_data_typeof_str = "Vector2"
			TYPE_RECT2:
				property_data_typeof_str = "Rect2"
			TYPE_VECTOR3:
				property_data_typeof_str = "Vector3"
			TYPE_TRANSFORM2D:
				property_data_typeof_str = "Transform2D"
			TYPE_PLANE:
				property_data_typeof_str = "Plane"
			TYPE_QUAT:
				property_data_typeof_str = "Quat"
			TYPE_AABB:
				property_data_typeof_str = "AABB"
			TYPE_BASIS:
				property_data_typeof_str = "Basis"
			TYPE_TRANSFORM:
				property_data_typeof_str = "Transform"
			TYPE_COLOR:
				property_data_typeof_str = "Color"
			TYPE_NODE_PATH:
				property_data_typeof_str = "NodePath"
			TYPE_RID:
				property_data_typeof_str = "RID"
			TYPE_OBJECT:
				property_data_typeof_str = "Object"
			TYPE_DICTIONARY:
				property_data_typeof_str = "Dictionary"
			TYPE_ARRAY:
				property_data_typeof_str = "Array"
			TYPE_RAW_ARRAY:
				property_data_typeof_str = "PoolByteArray"
			TYPE_INT_ARRAY:
				property_data_typeof_str = "PoolIntArray"
			TYPE_REAL_ARRAY:
				property_data_typeof_str = "PoolRealArray"
			TYPE_STRING_ARRAY:
				property_data_typeof_str = "PoolStringArray"
			TYPE_VECTOR2_ARRAY:
				property_data_typeof_str = "PoolVector2Array"
			TYPE_VECTOR3_ARRAY:
				property_data_typeof_str = "PoolVector3Array"
			TYPE_COLOR_ARRAY:
				property_data_typeof_str = "PoolColorArray"
			TYPE_MAX:
				property_data_typeof_str = "Variant"
			_:
				property_data_typeof_str = "?"
		if (property_data_typeof == TYPE_ARRAY):
			_print_if_verbose(tabulation_level, property.name, ":", property_data_typeof_str, ", size is ", data.get(property.name).size())
			var data_array = data.get(property.name)
			for i in range(data_array.size()):
				if (typeof(data_array[i]) == TYPE_OBJECT):
					_print_data_class_info(data_array[i], tabulation_level + "\t[" + str(i) + "]", tabulation_level + "\t\t")
				else:
					_print_if_verbose(tabulation_level + "\t", "[", i, "] = ", data_array[i])
		elif (property_data_typeof == TYPE_OBJECT):
			_print_data_class_info(data.get(property.name), tabulation_level + property.name, tabulation_level + "\t")
		else:
			_print_if_verbose(tabulation_level, property.name, ":", property_data_typeof_str, " = ", property_data_str)

